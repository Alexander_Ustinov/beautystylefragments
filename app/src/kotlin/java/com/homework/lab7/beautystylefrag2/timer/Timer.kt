package com.homework.lab7.beautystylefrag2.timer

interface Timer {
    fun start()
    fun stop()
    fun reset()
    fun runTimer(consumer: (String) -> Unit)
    fun isRunning(): Boolean
    fun getSeconds(): Int
    fun setSeconds(seconds: Int)
}