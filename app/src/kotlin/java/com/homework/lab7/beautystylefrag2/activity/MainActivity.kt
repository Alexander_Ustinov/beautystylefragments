package com.homework.lab7.beautystylefrag2.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.LinearLayout
import com.homework.lab7.beautystylefrag2.R
import com.homework.lab7.beautystylefrag2.ApplicationPart
import com.homework.lab7.beautystylefrag2.IntentValue


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initHairViews()
        initNailViews()
    }

    private fun initHairViews() {
        val hairViews = findViewById<LinearLayout>(R.id.hair_views)
        hairViews.setOnClickListener {
            val intent = Intent(this, FrameItemActivity::class.java)
            intent.putExtra(IntentValue.FRAGMENT_LIST.name, ApplicationPart.HAIRS)
            startActivity(intent)
        }
    }

    private fun initNailViews() {
        val hairViews = findViewById<LinearLayout>(R.id.nail_views)
        hairViews.setOnClickListener {
            val intent = Intent(this, FrameItemActivity::class.java)
            intent.putExtra(IntentValue.FRAGMENT_LIST.name, ApplicationPart.NAILS)
            startActivity(intent)
        }
    }
}
