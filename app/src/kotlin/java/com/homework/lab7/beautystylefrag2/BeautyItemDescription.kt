package com.homework.lab7.beautystylefrag2

import android.os.Parcel
import android.os.Parcelable

class BeautyItemDescription (val name: String, val description: String, val imageId: Int) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeInt(imageId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BeautyItemDescription> {
        override fun createFromParcel(parcel: Parcel): BeautyItemDescription {
            return BeautyItemDescription(parcel)
        }

        override fun newArray(size: Int): Array<BeautyItemDescription?> {
            return arrayOfNulls(size)
        }
    }
}