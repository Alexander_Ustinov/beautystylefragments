package com.homework.lab7.beautystylefrag2

enum class ApplicationPart {
    NAILS,
    HAIRS
}