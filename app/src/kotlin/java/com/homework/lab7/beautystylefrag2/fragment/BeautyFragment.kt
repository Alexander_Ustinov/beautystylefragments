package com.homework.lab7.beautystylefrag2.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.homework.lab7.beautystylefrag2.BeautyItemDescription

import com.homework.lab7.beautystylefrag2.R


class BeautyFragment : Fragment() {

    private var name: String? = null
    private var description: String? = null
    private var imageId: Int = 0
    private var fragmentView: View? = null


    companion object {
        private const val FRAGMENT_DESCRIPTION_DATA = "fragment_description_data"

        fun getNewInstance(name: String, description: String, imageId: Int): BeautyFragment {
            val fragment = BeautyFragment()
            val arguments = Bundle()
            val descriptionData = BeautyItemDescription(name, description, imageId)
            arguments.putParcelable(FRAGMENT_DESCRIPTION_DATA, descriptionData)
            fragment.arguments = arguments
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_beauty_detail, container, false)
        if (arguments != null) {
            loadData(arguments)
        }
        return fragmentView
    }

    private fun loadData(bundle: Bundle?) {
        val descriptionData = bundle!!.getParcelable<BeautyItemDescription>(FRAGMENT_DESCRIPTION_DATA)
        name = descriptionData.name
        description = descriptionData.description
        imageId = descriptionData.imageId
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadArguments()
        initTimerFragment()
        fillFragmentViewsWithData()
    }


    private fun loadArguments() {
        if (arguments != null) {
            if (arguments!!.containsKey(FRAGMENT_DESCRIPTION_DATA)) {
                loadDataFormParcelableDescription(arguments!!)
            }
        } else {
            throw IllegalArgumentException("Must be created through newInstance(...)")
        }
    }

    private fun loadDataFormParcelableDescription(arguments: Bundle) {
        val descriptionData = arguments.getParcelable<BeautyItemDescription>(FRAGMENT_DESCRIPTION_DATA)
        if (descriptionData != null) {
            name = descriptionData.name
            description = descriptionData.description
            imageId = descriptionData.imageId
        }
    }

    private fun initTimerFragment() {
        val timerFragmentTransaction = childFragmentManager.beginTransaction()
        timerFragmentTransaction.replace(R.id.clock_container, ClockFragment())
        timerFragmentTransaction.addToBackStack(null)
        timerFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        timerFragmentTransaction.commit()
    }

    private fun fillFragmentViewsWithData() {
        val nameView = fragmentView!!.findViewById<TextView>(R.id.item_detail_name)
        val descriptionView = fragmentView!!.findViewById<TextView>(R.id.item_detail_description)
        val imageView = fragmentView!!.findViewById<ImageView>(R.id.item_detail_image)
        nameView.text = name
        descriptionView.text = description
        imageView.setImageResource(imageId)
    }
}
