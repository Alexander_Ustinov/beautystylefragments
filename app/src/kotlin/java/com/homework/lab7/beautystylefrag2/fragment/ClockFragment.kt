package com.homework.lab7.beautystylefrag2.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.homework.lab7.beautystylefrag2.R
import com.homework.lab7.beautystylefrag2.TimerData
import com.homework.lab7.beautystylefrag2.TimerParameter
import com.homework.lab7.beautystylefrag2.timer.SecondTimer
import com.homework.lab7.beautystylefrag2.timer.Timer

class ClockFragment : Fragment() {

    private lateinit var secondsTimer: Timer
    private var wasRunning: Boolean = false
    private lateinit var fragmentView: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_clock, container, false)
        return fragmentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initTimer()
        loadArguments(savedInstanceState)
        drawTime()
        initListeners()
    }


    private fun initTimer() {
        secondsTimer = SecondTimer()
    }

    private fun loadArguments(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(TimerData.TIMER_SAVED_DATA.toString())) {
                loadDataFromParcelable(savedInstanceState)
            }
        }
    }

    private fun loadDataFromParcelable(arguments: Bundle?) {
        var loadedParameters = arguments!!.getParcelable<TimerParameter>(TimerData.TIMER_SAVED_DATA.toString())
        if (loadedParameters != null) {
            secondsTimer.setSeconds(loadedParameters.seconds)
            if (loadedParameters.isRunning) {
                secondsTimer.start()
            } else {
                secondsTimer.stop()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        if (wasRunning) {
            secondsTimer.start()
        }
    }

    override fun onStop() {
        super.onStop()
        wasRunning = secondsTimer.isRunning()
        secondsTimer.stop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(TimerData.TIMER_SAVED_DATA.toString(), TimerParameter(secondsTimer.getSeconds(), secondsTimer.isRunning()))
        super.onSaveInstanceState(outState)
    }

    private fun drawTime() {
        var timeView = fragmentView.findViewById<TextView>(R.id.textView)
        secondsTimer.runTimer { text -> timeView.text = text}
    }

    private fun initListeners() {
        fragmentView.findViewById<Button>(R.id.buttonReset).setOnClickListener {
            secondsTimer.stop()
            secondsTimer.reset()
        }

        fragmentView.findViewById<Button>(R.id.buttonStart).setOnClickListener{secondsTimer.start()}
        fragmentView.findViewById<Button>(R.id.buttonStop).setOnClickListener{secondsTimer.stop()}
    }

}
