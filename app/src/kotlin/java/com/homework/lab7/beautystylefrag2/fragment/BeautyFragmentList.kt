package com.homework.lab7.beautystylefrag2.fragment

import android.os.Bundle
import android.support.v4.app.ListFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import com.homework.lab7.beautystylefrag2.MainListItem
import com.homework.lab7.beautystylefrag2.R


class BeautyFragmentList : ListFragment() {

    private var itemList: List<String>? = null
    var itemData: Array<MainListItem>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        initData()
        initMainListAdapter(inflater!!)
        return inflater.inflate(R.layout.fragment_beauty_list, container, false)
    }

    private fun initData() {
        itemList = itemData!!.map { item -> item.name }.toList()
    }

    private fun initMainListAdapter(inflater: LayoutInflater) {
        val mainListAdapter = ArrayAdapter(inflater.context,
                android.R.layout.simple_list_item_1, itemList!!)
        listAdapter = mainListAdapter
    }

    override fun onListItemClick(l: ListView?, v: View?, position: Int, id: Long) {
        super.onListItemClick(l, v, position, id)
        initDetailFragment(getDetailFragmentWithData(position))
    }

    private fun initDetailFragment(nextFragment: BeautyFragment) {
        activity!!.supportFragmentManager.beginTransaction()
                .replace(R.id.main_fragment_container, nextFragment).addToBackStack(null).commit()
    }

    private fun getDetailFragmentWithData(dataIndex: Int): BeautyFragment {
        return BeautyFragment.getNewInstance(
                itemData!![dataIndex].name,
                itemData!![dataIndex].description,
                itemData!![dataIndex].imageResourceId
        )
    }
}
