package com.homework.lab7.beautystylefrag2

import android.os.Parcel
import android.os.Parcelable

class TimerParameter() : Parcelable {

    var seconds = 0
    var isRunning = false

    constructor(parcel: Parcel) : this() {
        seconds = parcel.readInt()
        isRunning = parcel.readInt() != 0
    }

    constructor(seconds: Int, isRunning: Boolean) : this() {
        this.seconds = seconds
        this.isRunning = isRunning
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TimerParameter> {
        override fun createFromParcel(parcel: Parcel): TimerParameter {
            return TimerParameter(parcel)
        }

        override fun newArray(size: Int): Array<TimerParameter?> {
            return arrayOfNulls(size)
        }
    }
}