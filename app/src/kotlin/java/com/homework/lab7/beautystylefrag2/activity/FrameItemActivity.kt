package com.homework.lab7.beautystylefrag2.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.homework.lab7.beautystylefrag2.R
import com.homework.lab7.beautystylefrag2.ApplicationPart
import com.homework.lab7.beautystylefrag2.IntentValue
import com.homework.lab7.beautystylefrag2.MainListItem
import com.homework.lab7.beautystylefrag2.MainListItemHolder
import com.homework.lab7.beautystylefrag2.fragment.BeautyFragmentList


class FrameItemActivity : AppCompatActivity() {

    private var listData: Array<MainListItem>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_frame_item)
        loadData(loadApplicationPartFromIntent())
        savedInstanceState ?: showListFragment()
    }

    private fun loadApplicationPartFromIntent(): ApplicationPart {
        return ApplicationPart.valueOf(intent.getSerializableExtra(IntentValue.FRAGMENT_LIST.name).toString())
    }

    private fun loadData(applicationPart: ApplicationPart) {
        listData = when (applicationPart) {
            ApplicationPart.NAILS -> MainListItemHolder.nailListItems
            ApplicationPart.HAIRS -> MainListItemHolder.hairListItems
        }
    }

    private fun showListFragment() {
        val simpleListFragment = BeautyFragmentList()
        simpleListFragment.itemData = listData
        supportFragmentManager.beginTransaction()
                .replace(R.id.main_fragment_container, simpleListFragment).commit()
    }
}
