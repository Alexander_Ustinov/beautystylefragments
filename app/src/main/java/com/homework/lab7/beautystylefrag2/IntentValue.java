package com.homework.lab7.beautystylefrag2;

public enum IntentValue {
    FRAGMENT_LIST("fragment_list");

    private String name;

    IntentValue(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
