package com.homework.lab7.beautystylefrag2;

import android.os.Parcel;
import android.os.Parcelable;

public class BeautyItemDescription implements Parcelable {

    private String name;
    private String description;
    private int imageId;

    public BeautyItemDescription(String name, String description, int imageId) {
        this.name = name;
        this.description = description;
        this.imageId = imageId;
    }

    protected BeautyItemDescription(Parcel in) {
        name = in.readString();
        description = in.readString();
        imageId = in.readInt();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public static final Creator<BeautyItemDescription> CREATOR = new Creator<BeautyItemDescription>() {
        @Override
        public BeautyItemDescription createFromParcel(Parcel in) {
            return new BeautyItemDescription(in);
        }

        @Override
        public BeautyItemDescription[] newArray(int size) {
            return new BeautyItemDescription[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeInt(imageId);
    }
}
