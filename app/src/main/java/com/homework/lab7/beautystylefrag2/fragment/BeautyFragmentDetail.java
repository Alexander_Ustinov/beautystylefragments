package com.homework.lab7.beautystylefrag2.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.homework.lab7.beautystylefrag2.BeautyItemDescription;
import com.homework.lab7.beautystylefrag2.R;

import java.util.Objects;

public class BeautyFragmentDetail extends Fragment {

    private static final String FRAGMENT_DESCRIPTION_DATA = "fragment_item_description";

    private String name;
    private String description;
    private int imageId;
    private View fragmentView;

    public static BeautyFragmentDetail getNewInstance(final String fragmentName,
                                                      final String fragmentDescription,
                                                      final int fragmentImageId) {
        final BeautyFragmentDetail fragment = new BeautyFragmentDetail();
        final Bundle arguments = new Bundle();
        final BeautyItemDescription descriptionData =
                new BeautyItemDescription(fragmentName, fragmentDescription, fragmentImageId);
        arguments.putParcelable(FRAGMENT_DESCRIPTION_DATA, descriptionData);
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.fragment_beauty_detail, container, false);
        return fragmentView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);setRetainInstance(true);
        loadArguments();
        initTimerFragment();
        fillFragmentViewsWithData();
    }

    private void loadArguments() {
        if (getArguments() != null) {
            if (getArguments().containsKey(FRAGMENT_DESCRIPTION_DATA)) {
                loadDataFormParcelableDescription(getArguments());
            }
        } else {
            throw new IllegalArgumentException("Must be created through newInstance(...)");
        }
    }

    private void loadDataFormParcelableDescription(Bundle arguments) {
        final BeautyItemDescription descriptionData = arguments.getParcelable(FRAGMENT_DESCRIPTION_DATA);
        if (descriptionData != null) {
            name = descriptionData.getName();
            description = descriptionData.getDescription();
            imageId = descriptionData.getImageId();
        }
    }

    private void initTimerFragment() {
        FragmentTransaction timerFragmentTransaction = getChildFragmentManager().beginTransaction();
        timerFragmentTransaction.replace(R.id.clock_container, new ClockFragment());
        timerFragmentTransaction.addToBackStack(null);
        timerFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        timerFragmentTransaction.commit();
    }

    private void fillFragmentViewsWithData() {
        TextView nameView = Objects.requireNonNull(fragmentView.findViewById(R.id.item_detail_name));
        TextView descriptionView = Objects.requireNonNull(fragmentView.findViewById(R.id.item_detail_description));
        ImageView imageView = Objects.requireNonNull(fragmentView.findViewById(R.id.item_detail_image));
        nameView.setText(name);
        descriptionView.setText(description);
        imageView.setImageResource(imageId);
    }
}
