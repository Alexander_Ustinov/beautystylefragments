package com.homework.lab7.beautystylefrag2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.homework.lab7.beautystylefrag2.ApplicationPart;
import com.homework.lab7.beautystylefrag2.IntentValue;
import com.homework.lab7.beautystylefrag2.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initAllViews();
    }

    private void initAllViews() {
        initNailViews();
        initHairViews();
    }

    private void initNailViews() {
        LinearLayout nailViews = findViewById(R.id.nail_views);
        nailViews.setOnClickListener(createClickListener(ApplicationPart.NAILS));
    }

    private void initHairViews() {
        LinearLayout hairViews = findViewById(R.id.hair_views);
        hairViews.setOnClickListener(createClickListener(ApplicationPart.HAIRS));
    }

    private View.OnClickListener createClickListener(ApplicationPart applicationPart) {
        return v -> {
            Intent intent = new Intent(MainActivity.this, FrameActivity.class);
            intent.putExtra(IntentValue.FRAGMENT_LIST.getName(), applicationPart);
            startActivity(intent);
        };
    }
}
