package com.homework.lab7.beautystylefrag2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.homework.lab7.beautystylefrag2.ApplicationPart;
import com.homework.lab7.beautystylefrag2.IntentValue;
import com.homework.lab7.beautystylefrag2.MainListItem;
import com.homework.lab7.beautystylefrag2.MainListItemHolder;
import com.homework.lab7.beautystylefrag2.R;
import com.homework.lab7.beautystylefrag2.fragment.BeautyListFragment;

public class FrameActivity extends AppCompatActivity {

    private MainListItem[] listData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame);
        loadData(loadApplicationPartFromIntent());
        if (savedInstanceState == null) {
            showListFragment();
        }
    }

    private ApplicationPart loadApplicationPartFromIntent() {
        Intent intent = getIntent();
        return ApplicationPart.valueOf(
                intent.getSerializableExtra(IntentValue.FRAGMENT_LIST.getName()).toString()
        );
    }

    private void loadData(ApplicationPart applicationPart) {
        switch (applicationPart) {
            case NAILS:
                listData = MainListItemHolder.nailListItems;
                break;
            case HAIRS:
                listData = MainListItemHolder.hairListItems;
                break;
        }
    }

    private void showListFragment() {
        BeautyListFragment simpleListFragment = new BeautyListFragment();
        simpleListFragment.setItemData(listData);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_fragment_container, simpleListFragment).commit();
    }
}
