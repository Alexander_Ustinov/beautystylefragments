package com.homework.lab7.beautystylefrag2;

public class MainListItemHolder {

    public static final MainListItem[] nailListItems = {
            new MainListItem("Classic manicure",
                    "This is classic manicure", R.drawable.emma),
            new MainListItem("Classic pedicure",
                    "This is classic pedicure", R.drawable.emma),
            new MainListItem("Hardware manicure",
                    "This is hardware manicure", R.drawable.emma)
    };

    public static final MainListItem[] hairListItems = {
            new MainListItem("Hair Limb Loosener",
                    "5 Handstand push-ups\n10 1-legged squats\n15 Pull-ups", R.drawable.emma),
            new MainListItem("Hair Agony",
                    "100 Pull-ups\n100 Push-ups\n100 Sit-ups\n100 Squats", R.drawable.emma),
            new MainListItem("The Wimp Special",
                    "5 Pull-ups\n10 Push-ups\n15 Squats", R.drawable.emma)
    };
}
