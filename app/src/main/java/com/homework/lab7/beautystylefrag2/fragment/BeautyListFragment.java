package com.homework.lab7.beautystylefrag2.fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.homework.lab7.beautystylefrag2.MainListItem;
import com.homework.lab7.beautystylefrag2.R;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class BeautyListFragment extends ListFragment {

    private List<String> itemList;
    private MainListItem[] itemData;

    public void setItemData(MainListItem[] itemData) {
        this.itemData = itemData;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        initData();
        initMainListAdapter(inflater);
        return inflater.inflate(R.layout.fragment_beauty_list, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void initData() {
        itemList = Arrays.stream(itemData)
                .map(MainListItem::getName).collect(Collectors.toList());
        itemData = itemData != null ? itemData : new MainListItem[0];
    }

    private void initMainListAdapter(LayoutInflater inflater) {
        ListAdapter mainListAdapter = new ArrayAdapter<>(inflater.getContext(),
                android.R.layout.simple_list_item_1, itemList);
        setListAdapter(mainListAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        initDetailFragment(getDetailFragmentWithData(position));
    }

    private void initDetailFragment(BeautyFragmentDetail nextFragment) {
        Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_fragment_container, nextFragment).addToBackStack(null).commit();
    }

    private BeautyFragmentDetail getDetailFragmentWithData(int dataIndex) {
        return BeautyFragmentDetail.getNewInstance(
                itemData[dataIndex].getName(),
                itemData[dataIndex].getDescription(),
                itemData[dataIndex].getImageResourceId()
        );
    }
}
