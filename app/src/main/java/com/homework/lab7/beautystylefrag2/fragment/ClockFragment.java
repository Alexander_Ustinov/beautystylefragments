package com.homework.lab7.beautystylefrag2.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.homework.lab7.beautystylefrag2.R;
import com.homework.lab7.beautystylefrag2.TimerData;
import com.homework.lab7.beautystylefrag2.TimerParameter;
import com.homework.lab7.beautystylefrag2.timer.SecondTimer;
import com.homework.lab7.beautystylefrag2.timer.Timer;

public class ClockFragment extends Fragment {

    private Timer secondTimer;
    private boolean wasRunning;

    private View fragmentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.fragment_clock, container, false);
        return fragmentView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initTimer();
        loadArguments(savedInstanceState);
        drawTime();
        initListeners();
    }

    private void initTimer() {
        secondTimer = new SecondTimer();
    }

    private void loadArguments(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(TimerData.TIMER_SAVED_DATA.name())) {
                loadDataFormParcelable(savedInstanceState);
            }
        }
    }

    private void loadDataFormParcelable(Bundle arguments) {
        final TimerParameter loadedParameters = arguments.getParcelable(TimerData.TIMER_SAVED_DATA.name());
        if (loadedParameters != null) {
            secondTimer.setSeconds(loadedParameters.getSeconds());
            if(loadedParameters.isRunning()) {
                secondTimer.start();
            } else {
                secondTimer.stop();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if(wasRunning) {
            secondTimer.start();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        wasRunning = secondTimer.isRunning();
        secondTimer.stop();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable(TimerData.TIMER_SAVED_DATA.name(),
                new TimerParameter(secondTimer.getSeconds(), secondTimer.isRunning()));
        super.onSaveInstanceState(outState);

    }

    private void drawTime() {
        final TextView timeView = fragmentView.findViewById(R.id.textView);
        secondTimer.runTimer(timeView::setText);
    }

    private void initListeners() {
        fragmentView.findViewById(R.id.buttonReset).setOnClickListener(
                button -> {
                    secondTimer.stop();
                    secondTimer.reset();
                });

        fragmentView.findViewById(R.id.buttonStart).setOnClickListener(button -> secondTimer.start());
        fragmentView.findViewById(R.id.buttonStop).setOnClickListener(button -> secondTimer.stop());
    }
}
