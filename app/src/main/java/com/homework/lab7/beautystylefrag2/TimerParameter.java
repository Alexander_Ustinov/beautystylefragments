package com.homework.lab7.beautystylefrag2;

import android.os.Parcel;
import android.os.Parcelable;

public class TimerParameter implements Parcelable {

    private int seconds;
    private boolean isRunning;

    protected TimerParameter(Parcel in) {
        seconds = in.readInt();
        isRunning = in.readInt() != 0;
    }

    public TimerParameter(int seconds, boolean isRunning) {
        this.seconds = seconds;
        this.isRunning = isRunning;
    }

    public static final Creator<TimerParameter> CREATOR = new Creator<TimerParameter>() {
        @Override
        public TimerParameter createFromParcel(Parcel in) {
            return new TimerParameter(in);
        }

        @Override
        public TimerParameter[] newArray(int size) {
            return new TimerParameter[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }

}
